﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Consoltica.Startup))]
namespace Consoltica
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
